“ Vim color file - chela_light_modified
“ Generated by http://bytefluent.com/vivify 2020-10-04
set background=light
if version > 580
	hi clear
	if exists(“syntax_on”)
		syntax reset
	endif
endif

set t_Co=256
let g:colors_name = “chela_light_modified”

“hi SignColumn — no settings —
“hi CTagsMember — no settings —
“hi CTagsGlobalConstant — no settings —
“hi Ignore — no settings —
hi Normal guifg=#595959 guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=240 ctermbg=15 cterm=NONE
“hi CTagsImport — no settings —
“hi CTagsGlobalVariable — no settings —
“hi SpellRare — no settings —
“hi EnumerationValue — no settings —
“hi Float — no settings —
“hi Union — no settings —
“hi VisualNOS — no settings —
“hi EnumerationName — no settings —
“hi SpellCap — no settings —
“hi SpellLocal — no settings —
“hi DefinedName — no settings —
“hi LocalVariable — no settings —
“hi SpellBad — no settings —
“hi CTagsClass — no settings —
“hi clear — no settings —
hi IncSearch guifg=#373737 guibg=#00ff11 guisp=#00ff11 gui=NONE ctermfg=237 ctermbg=10 cterm=NONE
hi WildMenu guifg=#b3b3b3 guibg=#009999 guisp=#009999 gui=NONE ctermfg=249 ctermbg=30 cterm=NONE
hi SpecialComment guifg=#b33c00 guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=130 ctermbg=15 cterm=NONE
hi Typedef guifg=#b3187f guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=126 ctermbg=15 cterm=NONE
hi Title guifg=#b3488f guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=132 ctermbg=15 cterm=NONE
hi Folded guifg=#b3187f guibg=#eeeeee guisp=#eeeeee gui=NONE ctermfg=126 ctermbg=255 cterm=NONE
hi PreCondit guifg=#b33c00 guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=130 ctermbg=15 cterm=NONE
hi Include guifg=#b3187f guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=126 ctermbg=15 cterm=NONE
hi TabLineSel guifg=#b3b3b3 guibg=#ff22b5 guisp=#ff22b5 gui=NONE ctermfg=249 ctermbg=199 cterm=NONE
hi StatusLineNC guifg=#b3b3b3 guibg=#ff22b5 guisp=#ff22b5 gui=NONE ctermfg=249 ctermbg=199 cterm=NONE
hi NonText guifg=#b3b3b3 guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=249 ctermbg=15 cterm=NONE
hi DiffText guifg=#373737 guibg=#00ff55 guisp=#00ff55 gui=NONE ctermfg=237 ctermbg=47 cterm=NONE
hi ErrorMsg guifg=#b3b3b3 guibg=#b5ff22 guisp=#b5ff22 gui=NONE ctermfg=249 ctermbg=154 cterm=NONE
hi Debug guifg=#b33c00 guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=130 ctermbg=15 cterm=NONE
hi PMenuSbar guifg=NONE guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=NONE ctermbg=15 cterm=NONE
hi Identifier guifg=#b3187f guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=126 ctermbg=15 cterm=NONE
hi SpecialChar guifg=#81b31d guibg=#fafafa guisp=#fafafa gui=underline ctermfg=106 ctermbg=15 cterm=underline
hi Conditional guifg=#b3187f guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=126 ctermbg=15 cterm=NONE
hi StorageClass guifg=#b3187f guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=126 ctermbg=15 cterm=NONE
hi Todo guifg=#b3b3b3 guibg=#337799 guisp=#337799 gui=NONE ctermfg=249 ctermbg=67 cterm=NONE
hi Special guifg=#b33c00 guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=130 ctermbg=15 cterm=NONE
hi LineNr guifg=#b3b3b3 guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=249 ctermbg=15 cterm=NONE
hi StatusLine guifg=#b3b3b3 guibg=#ff22b5 guisp=#ff22b5 gui=NONE ctermfg=249 ctermbg=199 cterm=NONE
hi Label guifg=#b3187f guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=126 ctermbg=15 cterm=NONE
hi PMenuSel guifg=#b3b3b3 guibg=#ff22b5 guisp=#ff22b5 gui=NONE ctermfg=249 ctermbg=199 cterm=NONE
hi Search guifg=#373737 guibg=#00ff55 guisp=#00ff55 gui=NONE ctermfg=237 ctermbg=47 cterm=NONE
hi Delimiter guifg=#b33c00 guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=130 ctermbg=15 cterm=NONE
hi Statement guifg=#b3187f guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=126 ctermbg=15 cterm=NONE
hi Comment guifg=#00b3b3 guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=37 ctermbg=15 cterm=NONE
hi Character guifg=#81b31d guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=106 ctermbg=15 cterm=NONE
hi Number guifg=#81b31d guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=106 ctermbg=15 cterm=NONE
hi Boolean guifg=#b3187f guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=126 ctermbg=15 cterm=NONE
hi Operator guifg=#b3187f guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=126 ctermbg=15 cterm=NONE
hi CursorLine guifg=NONE guibg=#eeeeee guisp=#eeeeee gui=NONE ctermfg=NONE ctermbg=255 cterm=NONE
hi TabLineFill guifg=#b3b3b3 guibg=#aaaaaa guisp=#aaaaaa gui=NONE ctermfg=249 ctermbg=248 cterm=NONE
hi Question guifg=#b3b3b3 guibg=#009999 guisp=#009999 gui=NONE ctermfg=249 ctermbg=30 cterm=NONE
hi WarningMsg guifg=#77b300 guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=106 ctermbg=15 cterm=NONE
hi DiffDelete guifg=#8fb348 guibg=#ccff66 guisp=#ccff66 gui=NONE ctermfg=107 ctermbg=191 cterm=NONE
hi ModeMsg guifg=#b3b3b3 guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=249 ctermbg=15 cterm=NONE
hi CursorColumn guifg=NONE guibg=#eeeeee guisp=#eeeeee gui=NONE ctermfg=NONE ctermbg=255 cterm=NONE
hi Define guifg=#b3187f guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=126 ctermbg=15 cterm=NONE
hi Function guifg=#b3187f guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=126 ctermbg=15 cterm=NONE
hi FoldColumn guifg=#b3b3b3 guibg=#eeeeee guisp=#eeeeee gui=NONE ctermfg=249 ctermbg=255 cterm=NONE
hi PreProc guifg=#b3187f guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=126 ctermbg=15 cterm=NONE
hi Visual guifg=#6b6b6b guibg=#cccccc guisp=#cccccc gui=NONE ctermfg=242 ctermbg=252 cterm=NONE
hi MoreMsg guifg=#b3b3b3 guibg=#009999 guisp=#009999 gui=NONE ctermfg=249 ctermbg=30 cterm=NONE
hi VertSplit guifg=#b3187f guibg=#ff22b5 guisp=#ff22b5 gui=NONE ctermfg=126 ctermbg=199 cterm=NONE
hi Exception guifg=#b3187f guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=126 ctermbg=15 cterm=NONE
hi Keyword guifg=#b3187f guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=126 ctermbg=15 cterm=NONE
hi Type guifg=#b3187f guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=126 ctermbg=15 cterm=NONE
hi DiffChange guifg=#86b397 guibg=#00ff55 guisp=#00ff55 gui=NONE ctermfg=108 ctermbg=47 cterm=NONE
hi Cursor guifg=#b3b3b3 guibg=#000000 guisp=#000000 gui=NONE ctermfg=249 ctermbg=NONE cterm=NONE
hi Error guifg=#b3b3b3 guibg=#b5ff22 guisp=#b5ff22 gui=NONE ctermfg=249 ctermbg=154 cterm=NONE
hi PMenu guifg=#595959 guibg=#cccccc guisp=#cccccc gui=NONE ctermfg=240 ctermbg=252 cterm=NONE
hi SpecialKey guifg=#b33c00 guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=130 ctermbg=15 cterm=NONE
hi Constant guifg=#81b31d guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=106 ctermbg=15 cterm=NONE
hi Tag guifg=#b33c00 guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=130 ctermbg=15 cterm=NONE
hi String guifg=#81b31d guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=106 ctermbg=15 cterm=NONE
hi PMenuThumb guifg=NONE guibg=#cccccc guisp=#cccccc gui=NONE ctermfg=NONE ctermbg=252 cterm=NONE
hi MatchParen guifg=NONE guibg=#cccccc guisp=#cccccc gui=NONE ctermfg=NONE ctermbg=252 cterm=NONE
hi Repeat guifg=#b3187f guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=126 ctermbg=15 cterm=NONE
hi Directory guifg=#b3187f guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=126 ctermbg=15 cterm=NONE
hi Structure guifg=#b3187f guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=126 ctermbg=15 cterm=NONE
hi Macro guifg=#b3187f guibg=#fafafa guisp=#fafafa gui=NONE ctermfg=126 ctermbg=15 cterm=NONE
hi Underlined guifg=#b3187f guibg=#fafafa guisp=#fafafa gui=underline ctermfg=126 ctermbg=15 cterm=underline
hi DiffAdd guifg=#373737 guibg=#66ccff guisp=#66ccff gui=NONE ctermfg=237 ctermbg=81 cterm=NONE
hi TabLine guifg=#595959 guibg=#cccccc guisp=#cccccc gui=NONE ctermfg=240 ctermbg=252 cterm=NONE
hi cursorim guifg=#747474 guibg=#ff8bd8 guisp=#ff8bd8 gui=NONE ctermfg=243 ctermbg=212 cterm=NONE
hi mbenormal guifg=#97b395 guibg=#3f2e38 guisp=#3f2e38 gui=NONE ctermfg=108 ctermbg=237 cterm=NONE
hi perlspecialstring guifg=#b3776e guibg=#404040 guisp=#404040 gui=NONE ctermfg=131 ctermbg=238 cterm=NONE
hi doxygenspecial guifg=#6cb366 guibg=NONE guisp=NONE gui=NONE ctermfg=71 ctermbg=NONE cterm=NONE
hi mbechanged guifg=#b3b3b3 guibg=#3f2e38 guisp=#3f2e38 gui=NONE ctermfg=249 ctermbg=237 cterm=NONE
hi mbevisiblechanged guifg=#b3b3b3 guibg=#8f4e79 guisp=#8f4e79 gui=NONE ctermfg=249 ctermbg=96 cterm=NONE
hi doxygenparam guifg=#6cb366 guibg=NONE guisp=NONE gui=NONE ctermfg=71 ctermbg=NONE cterm=NONE
hi doxygensmallspecial guifg=#6cb366 guibg=NONE guisp=NONE gui=NONE ctermfg=71 ctermbg=NONE cterm=NONE
hi doxygenprev guifg=#6cb366 guibg=NONE guisp=NONE gui=NONE ctermfg=71 ctermbg=NONE cterm=NONE
hi perlspecialmatch guifg=#b3776e guibg=#404040 guisp=#404040 gui=NONE ctermfg=131 ctermbg=238 cterm=NONE
hi cformat guifg=#b3776e guibg=#404040 guisp=#404040 gui=NONE ctermfg=131 ctermbg=238 cterm=NONE
hi lcursor guifg=#747474 guibg=#8bdcff guisp=#8bdcff gui=NONE ctermfg=243 ctermbg=117 cterm=NONE
hi doxygenspecialmultilinedesc guifg=#24b30b guibg=NONE guisp=NONE gui=NONE ctermfg=34 ctermbg=NONE cterm=NONE
hi taglisttagname guifg=#b360a0 guibg=NONE guisp=NONE gui=NONE ctermfg=133 ctermbg=NONE cterm=NONE
hi doxygenbrief guifg=#57b343 guibg=NONE guisp=NONE gui=NONE ctermfg=71 ctermbg=NONE cterm=NONE
hi mbevisiblenormal guifg=#b3b3b1 guibg=#8f4e79 guisp=#8f4e79 gui=NONE ctermfg=249 ctermbg=96 cterm=NONE
hi user2 guifg=#b37ca1 guibg=#5e3e54 guisp=#5e3e54 gui=NONE ctermfg=139 ctermbg=59 cterm=NONE
hi user1 guifg=#0015b3 guibg=#5e3e54 guisp=#5e3e54 gui=NONE ctermfg=19 ctermbg=59 cterm=NONE
hi doxygenspecialonelinedesc guifg=#24b30b guibg=NONE guisp=NONE gui=NONE ctermfg=34 ctermbg=NONE cterm=NONE
hi doxygencomment guifg=#23b321 guibg=NONE guisp=NONE gui=NONE ctermfg=34 ctermbg=NONE cterm=NONE
hi cspecialcharacter guifg=#b3776e guibg=#404040 guisp=#404040 gui=NONE ctermfg=131 ctermbg=238 cterm=NONE
hi pythonimport guifg=#0077b3 guibg=NONE guisp=NONE gui=NONE ctermfg=31 ctermbg=NONE cterm=NONE
hi pythonexception guifg=#77b300 guibg=NONE guisp=NONE gui=NONE ctermfg=106 ctermbg=NONE cterm=NONE
hi pythonbuiltinfunction guifg=#0077b3 guibg=NONE guisp=NONE gui=NONE ctermfg=31 ctermbg=NONE cterm=NONE
hi pythonoperator guifg=#b38bb3 guibg=NONE guisp=NONE gui=NONE ctermfg=139 ctermbg=NONE cterm=NONE
hi pythonexclass guifg=#0077b3 guibg=NONE guisp=NONE gui=NONE ctermfg=31 ctermbg=NONE cterm=NONE
hi stringdelimiter guifg=#50a088 guibg=NONE guisp=NONE gui=NONE ctermfg=72 ctermbg=NONE cterm=NONE
hi rubyregexp guifg=#b37700 guibg=NONE guisp=NONE gui=NONE ctermfg=136 ctermbg=NONE cterm=NONE
hi string guifg=#6eb39b guibg=NONE guisp=NONE gui=NONE ctermfg=72 ctermbg=NONE cterm=NONE
hi constant guifg=#73b342 guibg=NONE guisp=NONE gui=NONE ctermfg=107 ctermbg=NONE cterm=NONE
hi normal guifg=#a3b3a9 guibg=#151515 guisp=#151515 gui=NONE ctermfg=108 ctermbg=233 cterm=NONE
hi rubyinstancevariable guifg=#b38097 guibg=NONE guisp=NONE gui=NONE ctermfg=138 ctermbg=NONE cterm=NONE
hi rubyclass guifg=#984fb3 guibg=NONE guisp=NONE gui=NONE ctermfg=97 ctermbg=NONE cterm=NONE
hi identifier guifg=#b38998 guibg=NONE guisp=NONE gui=NONE ctermfg=138 ctermbg=NONE cterm=NONE
hi comment guifg=#b3b3b3 guibg=NONE guisp=NONE gui=italic ctermfg=249 ctermbg=NONE cterm=NONE
hi rubyregexpdelimiter guifg=#971c00 guibg=NONE guisp=NONE gui=NONE ctermfg=88 ctermbg=NONE cterm=NONE
hi rubyregexpspecial guifg=#b37100 guibg=NONE guisp=NONE gui=NONE ctermfg=130 ctermbg=NONE cterm=NONE
hi rubypredefinedidentifier guifg=#aab343 guibg=NONE guisp=NONE gui=NONE ctermfg=143 ctermbg=NONE cterm=NONE
hi function guifg=#57b357 guibg=NONE guisp=NONE gui=NONE ctermfg=71 ctermbg=NONE cterm=NONE
hi directory guifg=#6eb37e guibg=NONE guisp=NONE gui=NONE ctermfg=71 ctermbg=NONE cterm=NONE
hi rubysymbol guifg=#b263b3 guibg=NONE guisp=NONE gui=NONE ctermfg=133 ctermbg=NONE cterm=NONE
hi rubycontrol guifg=#ad6ab3 guibg=NONE guisp=NONE gui=NONE ctermfg=133 ctermbg=NONE cterm=NONE
hi rubyidentifier guifg=#b38097 guibg=NONE guisp=NONE gui=NONE ctermfg=138 ctermbg=NONE cterm=NONE
hi condtional guifg=#7e64b3 guibg=NONE guisp=NONE gui=NONE ctermfg=97 ctermbg=NONE cterm=NONE