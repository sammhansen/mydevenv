“ Vim color file - automation_modified
“ Generated by http://bytefluent.com/vivify 2020-10-04
set background=light
if version > 580
	hi clear
	if exists(“syntax_on”)
		syntax reset
	endif
endif

set t_Co=256
let g:colors_name = “automation_modified”

“hi SignColumn — no settings —
“hi Typedef — no settings —
“hi PreCondit — no settings —
“hi Include — no settings —
“hi TabLineSel — no settings —
“hi CTagsMember — no settings —
“hi CTagsGlobalConstant — no settings —
“hi Identifier — no settings —
“hi Conditional — no settings —
“hi StorageClass — no settings —
“hi Todo — no settings —
hi Normal guifg=#1a1a1a guibg=#f5f5f5 guisp=#f5f5f5 gui=NONE ctermfg=234 ctermbg=255 cterm=NONE
“hi CTagsImport — no settings —
“hi CTagsGlobalVariable — no settings —
“hi SpellRare — no settings —
“hi EnumerationValue — no settings —
“hi Float — no settings —
“hi Operator — no settings —
“hi CursorLine — no settings —
“hi Union — no settings —
“hi TabLineFill — no settings —
“hi CursorColumn — no settings —
“hi Define — no settings —
“hi Function — no settings —
“hi PreProc — no settings —
“hi EnumerationName — no settings —
“hi SpellCap — no settings —
“hi Exception — no settings —
“hi Keyword — no settings —
“hi Type — no settings —
“hi SpellLocal — no settings —
“hi Error — no settings —
“hi DefinedName — no settings —
“hi MatchParen — no settings —
“hi LocalVariable — no settings —
“hi Repeat — no settings —
“hi SpellBad — no settings —
“hi CTagsClass — no settings —
“hi Structure — no settings —
“hi Macro — no settings —
“hi Underlined — no settings —
“hi TabLine — no settings —
“hi clear — no settings —
hi IncSearch guifg=NONE guibg=NONE guisp=NONE gui=bold ctermfg=NONE ctermbg=NONE cterm=bold
hi WildMenu guifg=#1a1a1a guibg=#00d5ff guisp=#00d5ff gui=NONE ctermfg=234 ctermbg=45 cterm=NONE
hi SpecialComment guifg=#a41b00 guibg=#f5f5f5 guisp=#f5f5f5 gui=NONE ctermfg=124 ctermbg=255 cterm=NONE
hi Title guifg=#a41b00 guibg=NONE guisp=NONE gui=bold ctermfg=124 ctermbg=NONE cterm=bold
hi Folded guifg=#a41b00 guibg=#d3d3d3 guisp=#d3d3d3 gui=NONE ctermfg=124 ctermbg=252 cterm=NONE
hi StatusLineNC guifg=NONE guibg=NONE guisp=NONE gui=bold ctermfg=NONE ctermbg=NONE cterm=bold
hi NonText guifg=#15007e guibg=#cccccc guisp=#cccccc gui=bold ctermfg=18 ctermbg=252 cterm=bold
hi DiffText guifg=NONE guibg=#00ff2b guisp=#00ff2b gui=bold ctermfg=NONE ctermbg=10 cterm=bold
hi ErrorMsg guifg=#ffffff guibg=#00ff2b guisp=#00ff2b gui=NONE ctermfg=15 ctermbg=10 cterm=NONE
hi Ignore guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi Debug guifg=#a41b00 guibg=#f5f5f5 guisp=#f5f5f5 gui=NONE ctermfg=124 ctermbg=255 cterm=NONE
hi PMenuSbar guifg=NONE guibg=#d3d3d3 guisp=#d3d3d3 gui=NONE ctermfg=NONE ctermbg=252 cterm=NONE
hi SpecialChar guifg=#a41b00 guibg=#f5f5f5 guisp=#f5f5f5 gui=NONE ctermfg=124 ctermbg=255 cterm=NONE
hi Special guifg=#a41b00 guibg=#f5f5f5 guisp=#f5f5f5 gui=NONE ctermfg=124 ctermbg=255 cterm=NONE
hi LineNr guifg=#c3c3c3 guibg=#d3d3d3 guisp=#d3d3d3 gui=NONE ctermfg=7 ctermbg=252 cterm=NONE
hi StatusLine guifg=NONE guibg=NONE guisp=NONE gui=bold ctermfg=NONE ctermbg=NONE cterm=bold
hi Label guifg=#a41b00 guibg=NONE guisp=NONE gui=bold ctermfg=124 ctermbg=NONE cterm=bold
hi PMenuSel guifg=#a898f7 guibg=#989794 guisp=#989794 gui=NONE ctermfg=147 ctermbg=246 cterm=NONE
hi Search guifg=#1a1a1a guibg=#00d5ff guisp=#00d5ff gui=NONE ctermfg=234 ctermbg=45 cterm=NONE
hi Delimiter guifg=#a41b00 guibg=#f5f5f5 guisp=#f5f5f5 gui=NONE ctermfg=124 ctermbg=255 cterm=NONE
hi Statement guifg=#a41b00 guibg=NONE guisp=NONE gui=NONE ctermfg=124 ctermbg=NONE cterm=NONE
hi Comment guifg=#ff2b00 guibg=#e5e5e5 guisp=#e5e5e5 gui=NONE ctermfg=196 ctermbg=254 cterm=NONE
hi Character guifg=#a41b00 guibg=#f5f5f5 guisp=#f5f5f5 gui=NONE ctermfg=124 ctermbg=255 cterm=NONE
hi Number guifg=#a41b00 guibg=#f5f5f5 guisp=#f5f5f5 gui=NONE ctermfg=124 ctermbg=255 cterm=NONE
hi Boolean guifg=#a41b00 guibg=NONE guisp=NONE gui=NONE ctermfg=124 ctermbg=NONE cterm=NONE
hi Question guifg=#2b00ff guibg=NONE guisp=NONE gui=bold ctermfg=21 ctermbg=NONE cterm=bold
hi WarningMsg guifg=#00ff2b guibg=NONE guisp=NONE gui=NONE ctermfg=10 ctermbg=NONE cterm=NONE
hi VisualNOS guifg=NONE guibg=NONE guisp=NONE gui=bold,underline ctermfg=NONE ctermbg=NONE cterm=bold,underline
hi DiffDelete guifg=#ff2b00 guibg=#8b0074 guisp=#8b0074 gui=bold ctermfg=196 ctermbg=89 cterm=bold
hi ModeMsg guifg=NONE guibg=NONE guisp=NONE gui=bold ctermfg=NONE ctermbg=NONE cterm=bold
hi FoldColumn guifg=#a41b00 guibg=#bebebe guisp=#bebebe gui=NONE ctermfg=124 ctermbg=7 cterm=NONE
hi Visual guifg=#d8d8d8 guibg=#000000 guisp=#000000 gui=bold ctermfg=188 ctermbg=NONE cterm=bold
hi MoreMsg guifg=#7836a4 guibg=NONE guisp=NONE gui=bold ctermfg=97 ctermbg=NONE cterm=bold
hi VertSplit guifg=NONE guibg=NONE guisp=NONE gui=bold ctermfg=NONE ctermbg=NONE cterm=bold
hi DiffChange guifg=NONE guibg=#748b00 guisp=#748b00 gui=NONE ctermfg=NONE ctermbg=64 cterm=NONE
hi Cursor guifg=#1a1a1a guibg=#000000 guisp=#000000 gui=NONE ctermfg=234 ctermbg=NONE cterm=NONE
hi PMenu guifg=#f7f7f7 guibg=#585754 guisp=#585754 gui=NONE ctermfg=15 ctermbg=240 cterm=NONE
hi SpecialKey guifg=#a41b00 guibg=NONE guisp=NONE gui=NONE ctermfg=124 ctermbg=NONE cterm=NONE
hi Constant guifg=#a41b00 guibg=#f5f5f5 guisp=#f5f5f5 gui=NONE ctermfg=124 ctermbg=255 cterm=NONE
hi Tag guifg=#a41b00 guibg=#f5f5f5 guisp=#f5f5f5 gui=NONE ctermfg=124 ctermbg=255 cterm=NONE
hi String guifg=#15007e guibg=NONE guisp=NONE gui=NONE ctermfg=18 ctermbg=NONE cterm=NONE
hi PMenuThumb guifg=NONE guibg=#a9a9a9 guisp=#a9a9a9 gui=NONE ctermfg=NONE ctermbg=248 cterm=NONE
hi Directory guifg=#a41b00 guibg=NONE guisp=NONE gui=NONE ctermfg=124 ctermbg=NONE cterm=NONE
hi DiffAdd guifg=NONE guibg=#8b1700 guisp=#8b1700 gui=NONE ctermfg=NONE ctermbg=88 cterm=NONE
hi cursorim guifg=#3e2b35 guibg=#91535e guisp=#91535e gui=NONE ctermfg=237 ctermbg=95 cterm=NONE
hi lcursor guifg=#1a1a1a guibg=#ff00d5 guisp=#ff00d5 gui=NONE ctermfg=234 ctermbg=200 cterm=NONE