#!/bin/zsh
########################################
#|:====My Development Environment====:|#
#|:      Complete w/ Dotfiles        :|#
#|:          by Sam Hansen           :|#
#|:    Email: sam.hans24@yahoo.com   :|#
#|:----------------------------------:|#
########################################

HOME="/HOME/${USER}"

mkdir -p \
  $HOME/bin     \    
  $HOME/source  \ 
  $HOME/projects\ 
  $HOME/tmp     \  
  $HOME/source/github     \
  $HOME/source/gitlab     \         
  $HOME/source/bitbucket  \
  $HOME/source/github/repos \
  $HOME/source/github/gists \
  $HOME/.myfndir
  $HOME/.npm-global

sudo mkdir -p /vagrant/shared

# Apt Package Manager Installs
## TODO: NPM = trash-cli; OMZ = zsh-syntax-highlighting
sudo apt-get update -y && \
sudo apt-get install -y \
  zsh \
  wget \
  autotools \
  curl \
  git \
  xclip \
  python3 \
  python3-pip \
  build-essential \
  https-transport

# Oh-My-Zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# Update PATH to include use $USER/bin dir
export PATH=“$PATH:/bin:/usr/bin:/usr/local/bin:/sbin:/usr/local/sbin:~/.local/bin:~/.local/share/bin:~/bin”
echo “export PATH=“$PATH:/bin:/usr/bin:/usr/local/bin:/sbin:/usr/local/sbin:~/.local/bin:~/.local/share/bin:~/bin” | tee $HOME/.zshrc

echo “Updated PATH!”

# Install Micro
curl https://getmic.ro | bash ;
mv ~/micro ~/bin/micro ;
# Alternatively use snap
# sudo snap install micro --classic 

echo “Installed Micro Editor!”

# Install Node
curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash - ;
sudo apt-get install -y nodejs ;

echo “NodeJS eval(sh -c $(node --version)) Installed!”

# Install Yarn
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -\
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn ;
echo "export PATH='$PATH:/opt/yarn-1.3.2/bin'" | tee $HOME/.zshrc ;
echo "export PATH="$PATH:`yarn global bin`"" | tee $HOME/.zshrc ;

echo “Yarn Installed!” | lolcat

# Install Trash-CLI and Empty-Trash-CLI
npm i -g trash-cli empty-trash-cli 

# Powerline Fonts w/ package-managers
sudo apt-get install fonts-powerline 
sudo pip3 install powerline-status 
# Powerline manually installed
git clone https://github.com/powerline/fonts.git 
cd fonts && ./install.sh 
wget https://github.com/powerline/powerline/raw/develop/font/PowerlineSymbols.otf \
wget https://github.com/powerline/powerline/raw/develop/font/10-powerline-symbols.conf 
mv PowerlineSymbols.otf ~/.local/share/fonts/ 
fc-cache -vf ~/.local/share/fonts/ 
mv 10-powerline-symbols.conf ~/.config/fontconfig/conf.d/ 

# Powerlevel9k Themeing
git clone “https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k 

# Adding Elvish Shell!
# Add Elvish PPA repo
sudo wget -O /etc/apt/trusted.gpg.d/elvish \
'https://sks-keyservers.net/pks/lookup?search=0xE9EA75D542E35A20&options=mr&op=get'
sudo gpg --dearmor /etc/apt/trusted.gpg.d/elvish;
sudo rm /etc/apt/trusted.gpg.d/elvish;
echo 'deb http://ppa.launchpad.net/zhsj/elvish/ubuntu xenial main' | \
sudo tee /etc/apt/sources.list.d/elvish.list
sudo apt-get update
# Instal Elvish
sudo apt-get install elvish

# Adding MySQL
# Make sure to download the package from 
# https://dev.mysql.com/downloads/repo/apt/ to /vagrant/shared
sudo dpkg -i /vagrant/shared/mysql-apt-config_0.8.13-1_all.deb
sudo apt-get update
sudo apt-get install mysql-server
echo “
#################################################################\
#                     !!Important!!                             #\
#                     #############                             #\
# Make sure you remember the root password you set. Users who   #\
# want to set a password later can leave the  password field    #\
# blank in the dialogue box and just press Ok; in that case,    #\
# root access to the server will be authenticated by Socket     #\
# Peer-Credential Pluggable Authentication for connections      #\
# using a Unix socket file. You can set the root password later #\
# password later using the program mysql_secure_installation.   #\
# You can check the status by using `sudo service mysql status` #\
# *Stop* the server with `sudo service mysql stop` and then     #\
# *Start* the server again with `sudo service mysql start`      #\
#################################################################\
“
# Install mysql packages
sudo apt-get install -y\
  mysql-community-test\
  libmysqlclient20\
  mysql-connector-python\ 
  mysql-connector-python-py3\        
  mysql-router 
# Add keys for secure updates on packages
gpg --recv-keys 5072E1F5
sudo apt-key adv --keyserver pgp.mit.edu --recv-keys 5072E1F5
# Add packages to apt-sources list
echo "deb http://repo.mysql.com/apt/ubuntu/ bionic mysql8.13" | \
sudo tee -a /etc/apt/sources.list.d/mysql.list
echo "deb http://repo.mysql.com/apt/ubuntu/ bionic mysql-community-test" | \
sudo tee -a /etc/apt/sources.list.d/mysql.list
echo "deb http://repo.mysql.com/apt/ubuntu/ bionic libmysqlclient20" | \
sudo tee -a /etc/apt/sources.list.d/mysql.list
echo "deb http://repo.mysql.com/apt/ubuntu/ bionic mysql-connector-python" | \
sudo tee -a /etc/apt/sources.list.d/mysql.list
echo "deb http://repo.mysql.com/apt/ubuntu/ bionic mysql-connector-python-py3" | \
sudo tee -a /etc/apt/sources.list.d/mysql.list
echo "deb http://repo.mysql.com/apt/ubuntu/ bionic mysql-router" | \
sudo tee -a /etc/apt/sources.list.d/mysql.list
sudo apt-get update -y;
